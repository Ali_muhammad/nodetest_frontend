import { Component, OnInit } from '@angular/core';
import { UserServicesService} from '../services/user-services.service';
import { from } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { debug } from 'util';
@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  public id: string;
  public name: string;
  public email: string;
  public location: string;
  public age: string;
  public gender: string;
  public message : String;
  constructor(private service:  UserServicesService, private rout: Router, 
    private activatedRoute: ActivatedRoute,
    private roastr: ToastrService) {
      this.activatedRoute.params.subscribe(params => {
        
        this.id = params.id;
      });
     }

  ngOnInit() {
     if(this.id) {
          this.getSingleUser();
        }
  }
  validation(){
    if(this.name == null || this.name == ''){
      this.roastr.warning("Name: ", "Name required" );
      return false;
    }
    if(this.email == null || this.email == ''){
      this.roastr.warning("Email: ", "Email required" );
      return false;
    }
    if(this.location == null || this.location == ''){
      this.roastr.warning("Location: ", "Locaton required" );
      return false;
    }
    if(this.age == null || this.age == ''){
      this.roastr.warning("age: ", "age required" );
      return false;
    }
    if(this.gender == null || this.gender == ''){
      this.roastr.warning("gender: ", "gender required" );
      return false;
    }
    return true;
  }
  addUser(){
    this.message = '';
    if(this.validation()){
   if(this.id) {
    const obj = {
      id: this.id,
      username: this.name,
      email: this.email,
      age: this.age,
      location: this.location,
      gender: this.gender
    }
    
     this.updateUser(obj);
   } else {
    const obj = {
      username: this.name,
      email: this.email,
      age: this.age,
      location: this.location,
      gender: this.gender
      }
     this.saveUser(obj);
    }
   }

  }

  saveUser(obj) {
    console.log(obj)
    this.service.addUser(obj).subscribe(
      (data: any)=>{
        console.log(data);
        this.name = '';
        this.email = '';
        this.location = '';
        this.age = '';
        this.gender = ''
        this.message = data.message;
        this.rout.navigate(['/'])
        this.roastr.success('user created')

      },
      (error)=>{
        console.log(error);
      }
    );
  }

  updateUser(obj) {
    this.service.updateuser(obj).subscribe(
      (data: any)=>{
        this.name = '';
        this.email = '';
        this.location = '';
        this.age = '';
        this.gender = '';
        this.message = data.message;
        this.rout.navigate(['/'])
        this.roastr.success('user updated')

      },
      (error)=>{
        console.log('error block');
        console.log(error);
      }
    );
  }
  getSingleUser() {
    if (this.id != null) {
      this.service.getUserById(this.id).subscribe(
        (data: any) => {
          console.log(data.data[0].username)
          this.name = data.data[0].username;
          this.location = data.data[0].location;
          this.age = data.data[0].age;
          this.email = data.data[0].email;
          this.gender = data.data[0].gender
          
        }, (error) => {

        }
      );
    }
  }
}
