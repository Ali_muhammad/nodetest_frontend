import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
// Import RxJs required methods


@Injectable({
  providedIn: 'root'
})
export class UserServicesService {

   private url = "http://localhost:3000/users/";
   private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'text/plain'
    })
  };
  constructor(private http: HttpClient) { }


  addUser(obj){
    console.log('service',obj)
    return this.http.post(this.url + 'adduser', obj);
    }

    getAllUser(){
      return this.http.get(this.url + 'userlist',this.httpOptions);
      }
    deleteuser(id){
      console.log('service delete ', id)
      return this.http.delete(this.url + 'deleteuser/' + id)
    }  
    updateuser(obj){
      console.log(obj)
      return this.http.patch(this.url + 'updateuser/'+obj.id ,obj)
    }

    getUserById(id){
      return this.http.get(this.url + 'oneuser/' + id);
      }
}
